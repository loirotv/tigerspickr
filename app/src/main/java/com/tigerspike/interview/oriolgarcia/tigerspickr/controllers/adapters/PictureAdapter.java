package com.tigerspike.interview.oriolgarcia.tigerspickr.controllers.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.tigerspike.interview.oriolgarcia.tigerspickr.MainActivity;
import com.tigerspike.interview.oriolgarcia.tigerspickr.R;
import com.tigerspike.interview.oriolgarcia.tigerspickr.models.PictureModel;

import java.util.ArrayList;

/**
 * Created by OriolGarcia on 13/7/17.
 */

public class PictureAdapter extends RecyclerView.Adapter {

    private MainActivity mActivity;
    private ArrayList data = new ArrayList();
    private Context mContext;

    public PictureAdapter(MainActivity mActivity, ArrayList data, Context mContext) {
        this.mActivity = mActivity;
        this.data = data;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_picture, parent, false);
        PictureHolder pictureHolder = new PictureHolder(view);
        return pictureHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder.getClass() == PictureHolder.class) {
            final PictureModel picture = (PictureModel) data.get(position);
            final PictureHolder ph = (PictureHolder) holder;
            Picasso.with(mActivity).load(picture.getImageUrl()).into(ph.pictureImageView);
        }
    }

    public static class PictureHolder extends RecyclerView.ViewHolder {
        LinearLayout pictureLayout;
        ImageView pictureImageView;

        public PictureHolder(View itemView) {
            super(itemView);
            pictureLayout = (LinearLayout) itemView.findViewById(R.id.item);
            pictureImageView = (ImageView) itemView.findViewById(R.id.picture_image_view);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}

