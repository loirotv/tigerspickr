package com.tigerspike.interview.oriolgarcia.tigerspickr;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.tigerspike.interview.oriolgarcia.tigerspickr.controllers.fragments.GalleryFragment;

public class MainActivity extends AppCompatActivity {

    private static final int GALLERY_FRAGMENT = 0;

    private FrameLayout mainFrameLayoutContainer;
    private Fragment mFragment;
    private FragmentManager fm = getFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainFrameLayoutContainer = (FrameLayout) findViewById(R.id.main_frame_layout_container);

        setFragment(GALLERY_FRAGMENT);
    }

    /**
     * Replaces main_frame_layout_container by the fragment (id) received by param
     * @param whichFragment
     */
    public void setFragment(int whichFragment) {
        FragmentTransaction ft = fm.beginTransaction();
        Fragment frag;

        switch (whichFragment) {
            case GALLERY_FRAGMENT:
                frag = new GalleryFragment();
                ft.replace(R.id.main_frame_layout_container, frag);
                ft.commit();
                setFragment(frag);
                break;
        }

    }

    /**
     * Changes variable mFragment by the fragment received by param
     * @param fragment
     */
    public void setFragment(android.app.Fragment fragment) {
        mFragment = fragment;
    }

}
