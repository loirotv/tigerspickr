package com.tigerspike.interview.oriolgarcia.tigerspickr.controllers.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tigerspike.interview.oriolgarcia.tigerspickr.MainActivity;

import de.greenrobot.event.EventBus;

/**
 * Created by OriolGarcia on 13/7/17.
 */

public class BaseFragment extends Fragment {

    protected View view;
    protected static MainActivity mActivity;
    protected Fragment mFragment, rFragment;

    public BaseFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
        mFragment = this;
        rFragment = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
/*
    @SuppressWarnings("unused")
    public void onEventMainThread(BaseErrorEvent error) {
        mActivity.showRedToast(getResources().getString(R.string.error_server_connection));Log.e("Error: ", (error.getDescription()));
    }
    */
}

